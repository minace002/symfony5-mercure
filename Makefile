OS := $(shell uname)
PATH_MERCURE := './bin/mercure'
ifeq ($(OS),Darwin)
		PATH_MERCURE := './bin/mercure_mac'
endif
mercure_server:
	JWT_KEY=my_test_key ADDR=localhost:3010 ALLOW_ANONYMOUS=1 CORS_ALLOWED_ORIGINS=* $(PATH_MERCURE)
