### Run mercure hub

`` /usr/bin/bash path_to_project/Makefile mercure_server ``

### Js

```javascript
const url = new URL('http://localhost:3010/.well-known/mercure');
url.searchParams.append('topic', 'test');
const eventSource = new EventSource(url);
eventSource.onmessage = event => {
    console.log(JSON.parse(event.data));
}
```

### Test url to push

```shell
http://localhost:8000/push?topic=test&data={"msg": 1}
```

### Test privat send event by Bearer token

Jwt key generated for topic=chat

```shell
http://localhost:8000/push-privat?topic=chat&data={"privat_msg": 1}
```

For generation new test key for custom topic [use jwt demo](https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOlsiKiJdfX0.iHLdpAEjX4BqCsHJEegxRmO-Y6sMxXwNATrQyRNt3GY)

PAYLOAD:

```json
{
  "mercure": {
    "publish": [
      "your_custom_topic"
    ],
    "subscribe": [
      "your_custom_topic"
    ]
  }
}
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mackrais Mercure test</title>
    <script src="https://cdn.jsdelivr.net/npm/event-source-polyfill@1.0.20"></script>
</head>
<body>

<script language="javascript">

    const urlParams = new URLSearchParams(window.location.search);

    let TOKEN = urlParams.get('token') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOlsiKiJdfX0.Mw2jb1e-DtDIn-MX-GggesmprxcDUkS3UiKuGDaoHsI';
    let TOPIC = urlParams.get('topic') || 'chat';
    let DOMAIN = urlParams.get('domain') || 'http://127.0.0.1:3010';

    console.log(`TOKEN ${TOKEN}\nTOPIC ${TOPIC}`)

    let EventSource = new EventSourcePolyfill(`${DOMAIN}/.well-known/mercure?topic=${TOPIC}`,  {
        headers: {
             "Authorization": `Bearer ${TOKEN}`
        }
    });

    EventSource.onmessage = event => {
        console.log(Date(), event);
    }

</script>

</body>
</html>
```

### Run php project

```shell
php -S localhost:8000 -t public/
```
