<?php

/**
 * Created by PhpStorm.
 * PHP Version: 7.4.
 *
 * @category
 *
 * @author     Oleh Boiko <boikoovdal@gmail.com>
 * @copyright  2014-2021 @Ovdaldk
 *
 * @see       <https://ovdal.dk>
 * @date      14.04.21
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    public function push(Request $request, HubInterface $hub)
    {
        $data = $request->get('data');
        $topic = $request->get('topic', 'default');
        $data = $data ? json_decode($data, true) : null;

        $update = new Update($topic, json_encode($data ?: ['message' => 'Hello world!']));
        $hub->publish($update);

        return $this->json(['result' => 'done', 'data' => $data ?: ['message' => 'Hello world!'], 'topic' => $topic]);
    }

    public function pushPrivat(Request $request, HubInterface $hub)
    {
        $data = $request->get('data');
        $topic = $request->get('topic', 'default');
        $data = $data ? json_decode($data, true) : null;

        $update = new Update($topic, json_encode($data ?: ['message' => 'Hello world!']), true);
        $hub->publish($update);

        return $this->json(['result' => 'done', 'data' => $data ?: ['message' => 'Hello world!'], 'topic' => $topic]);
    }

}
