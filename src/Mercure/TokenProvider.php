<?php

/**
 * Created by PhpStorm.
 * PHP Version: 7.4
 *
 * @category
 * @author     Oleh Boiko <boikoovdal@gmail.com>
 * @copyright  2014-2022 @Ovdaldk
 * @link       <https://ovdal.dk>
 * @date       1/25/22
 */

declare(strict_types=1);

namespace App\Mercure;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token\Builder;

final class TokenProvider
{
    private string $secret;

    public function __construct(string $secret) {
        $this->secret = $secret;
    }

    public function create(array $publish = []): string
    {
        $key = Key\InMemory::plainText($this->secret);
        $configuration = Configuration::forSymmetricSigner(new Sha256(), $key);

        return $configuration->builder()
            ->withClaim('mercure', ['publish' => $publish])
            ->getToken($configuration->signer(), $configuration->signingKey())
            ->toString();
    }

    public function __invoke(): string
    {
        return $this->create(['*']);
    }
}
